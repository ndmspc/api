FROM registry.gitlab.com/ndmspc/salsa:v0.7.0

RUN dnf install -y slurm sudo python3-pip
COPY ./requirements.txt /app/requirements.txt
RUN pip3 install --no-cache-dir -r /app/requirements.txt
COPY executor/ndmspc-executor.py /app
COPY executor/executor.py /app
COPY executor/template.yaml /app
# EXPOSE 3000
WORKDIR /app
RUN useradd -ms /usr/bin/bash executor
RUN dnf clean all
ENTRYPOINT ["/usr/bin/python3"]
CMD ["ndmspc-executor.py"]

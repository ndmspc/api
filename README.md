# api

## Executor

### Example POST request via cURL

```bash
curl -X POST \
    -H 'Content-Type: application/json' \
    -d '{"type":"salsa","subtype":"feeder","command":"sleep","args":"10"}' \
    http://localhost:3000
```

### Prerequisites

- `Node.js` installed
- `Python` installed
- `virtualenv` installed

### Why do we need `virtualenv` and how to install it?

- `pip install` installs packages/modules globally by default (that's bad)
- `virtualenv` creates special environment that allows installing pkgs/modules locally
- `virtualenv` can be install with `sudo pip install virtualenv`

### Starting FastAPI server

```bash
cd executor
virtualenv env # only on first setup
. env/bin/activate
pip install -r requirements.txt # only on first setup
python server.py
```

FastAPI documentation available at `/docs`

### Creating & publishing PyPI package

```bash
python setup.py sdist
twine upload dist/*
```

## Build docker image

```
podman build -t registry.gitlab.com/ndmspc/api:v0.20240209.0 .
podman push registry.gitlab.com/ndmspc/api:v0.20240209.0
```